package com.halalinaja.imark2.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResSuccess {
    @SerializedName("status")   @Expose private String status;
    @SerializedName("count")    @Expose private String count;
    @SerializedName("message")  @Expose private String message;

    public String getStatus() {
        return status;
    }

    public String getCount() {
        return count;
    }

    public String getMessage() {
        return message;
    }
}
