package com.halalinaja.imark2.Model;

import com.halalinaja.imark2.Model.ResUser;
import com.halalinaja.imark2.Model.ResDoctor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ResUser {
    @SerializedName("status")   @Expose private String status;
    @SerializedName("count")    @Expose private String count;
    @SerializedName("message")  @Expose private String message;
    @SerializedName("data")     @Expose private List<ResUserData> UserData;

    public String getStatus() {
        return status;
    }

    public String getCount() {
        return count;
    }

    public String getMessage() {
        return message;
    }

    public List<ResUserData> getUserData() {
        return UserData;
    }

    public void setUserData(ArrayList<ResUserData> UserData) {
        this.UserData = UserData;
    }
}
