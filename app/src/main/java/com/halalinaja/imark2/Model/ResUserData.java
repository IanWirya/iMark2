package com.halalinaja.imark2.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResUserData {
    @SerializedName("id")       @Expose private String id;
    @SerializedName("username") @Expose private String username;
    @SerializedName("mk_nama")  @Expose private String mkNama;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMkNama() {
        return mkNama;
    }

    public void setMkNama(String mkNama) {
        this.mkNama = mkNama;
    }
}
