package com.halalinaja.imark2.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ResDoctor {
    @SerializedName("status")   @Expose private String status;
    @SerializedName("count")    @Expose private String count;
    @SerializedName("message")  @Expose private String message;
    @SerializedName("data")     @Expose private List<ResDoctorData> DoctorData;

    public String getStatus() {
        return status;
    }

    public String getCount() {
        return count;
    }

    public String getMessage() {
        return message;
    }

    public List<ResDoctorData> getDoctorData() {
        return DoctorData;
    }

    public void setDoctorData(ArrayList<ResUserData> UserData) {
        this.DoctorData = DoctorData;
    }
}
