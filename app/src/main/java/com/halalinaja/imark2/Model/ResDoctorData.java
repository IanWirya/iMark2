package com.halalinaja.imark2.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResDoctorData {
    @SerializedName("kode_dokter")  @Expose private String kodeDokter;
    @SerializedName("nama_dokter")  @Expose private String namaDokter;
    @SerializedName("alamat_1")     @Expose private String alamat1;
    @SerializedName("alamat_2")     @Expose private String alamat2;
    @SerializedName("alamat_3")     @Expose private String alamat3;
    @SerializedName("total_ps")     @Expose private String totalPs;
    @SerializedName("total_omzet")  @Expose private String totalOmzet;
    @SerializedName("total_pasien") @Expose private String totalPasien;

    public ResDoctorData() {
        this.kodeDokter = kodeDokter;
        this.namaDokter = namaDokter;
        this.alamat1= alamat1;
        this.alamat2= alamat2;
        this.alamat3= alamat3;
        this.totalPs= totalPs;
        this.totalOmzet= totalOmzet;
        this.totalPasien= totalPasien;
    }

    public String getKodeDokter() {
        return kodeDokter;
    }

    public String getNamaDokter() {
        return namaDokter;
    }

    public String getAlamat1() {
        return alamat1;
    }

    public String getAlamat2() {
        return alamat2;
    }

    public String getAlamat3() {
        return alamat3;
    }

    public String getTotalPs() {
        return totalPs;
    }

    public String getTotalOmzet() {
        return totalOmzet;
    }

    public String getTotalPasien() {
        return totalPasien;
    }
}
