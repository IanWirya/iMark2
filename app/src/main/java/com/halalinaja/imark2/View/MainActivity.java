package com.halalinaja.imark2.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.halalinaja.imark2.AppConfig.AppAdapterSpinner;
import com.halalinaja.imark2.AppConfig.AppInterface;
import com.halalinaja.imark2.AppConfig.AppSessionManager;
import com.halalinaja.imark2.Model.ResCabang;
import com.halalinaja.imark2.Model.ResCabangData;
import com.halalinaja.imark2.Model.ResDoctor;
import com.halalinaja.imark2.Model.ResDoctorData;
import com.halalinaja.imark2.Model.ResError;
import com.halalinaja.imark2.R;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.halalinaja.imark2.AppConfig.AppGlobalVariable.APP_BASE_URL;

public class MainActivity extends AppCompatActivity {
    private TextView tvUsername;
    private EditText etNamaDokter;
    private Spinner spBulan, spTahun, spCabang;
    private View mMainFormView;
    private String account_username, account_name;
    private String sCabang;
    private AppSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Retrofit Call
        getCabangRequest();

        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarmain);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_main);
        getSupportActionBar().setTitle(getString(R.string.title_activity_main));

        //Session Login
        session = new AppSessionManager(getApplicationContext());
        HashMap<String, String> account = session.getUserDetails();
        account_username = account.get(AppSessionManager.KEY_USERNAME);
        account_name = account.get(AppSessionManager.KEY_NAME);

        //Date
        Calendar cal = Calendar.getInstance();
        int bulan = cal.get(Calendar.MONTH);
        int tahun = cal.get(Calendar.YEAR);

        //Form init
        tvUsername      = (TextView) findViewById(R.id.tvUsername);
        etNamaDokter    = (EditText) findViewById(R.id.etNamaDokter);
        spBulan         = (Spinner) findViewById(R.id.spBulan);
        spTahun         = (Spinner) findViewById(R.id.spTahun);
        spCabang        = (Spinner) findViewById(R.id.spCabang);
        mMainFormView   = findViewById(R.id.content_main);

        //Display Set to form
        tvUsername.setText("["+account_username+"] "+account_name);

        //Spinner Bulan
        String[] spBulanArray = {"Januari","Februari","Maret","April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
        ArrayAdapter<String> spBulanAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_dropdown_item_1line, spBulanArray);
        spBulan.setAdapter(spBulanAdapter);
        spBulan.setSelection(bulan);

        //Spinner Tahun
        Integer[] spTahunArray = {tahun-1, tahun, tahun+1};
        ArrayAdapter<Integer> spTahunAdapter = new ArrayAdapter<Integer>(MainActivity.this, android.R.layout.simple_dropdown_item_1line, spTahunArray);
        spTahun.setAdapter(spTahunAdapter);
        spTahun.setSelection(1); //set tahun ini

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSearch();
            }
        });
    }

    private void attemptSearch() {
        // Store values at the time of the login attempt.
        String user        = account_username;
        String nama_dokter = etNamaDokter.getText().toString();
        Integer dummy_bulan= spBulan.getSelectedItemPosition()+1;
        String bulan       = dummy_bulan.toString();
        String tahun       = spTahun.getSelectedItem().toString();
        String cab         = sCabang;
        String status      = "1";

        Intent i = new Intent(this, ListActivity.class);
        Bundle bundleSendSearchRequest = new Bundle();
        bundleSendSearchRequest.putString("user", user);
        bundleSendSearchRequest.putString("nama_dokter", nama_dokter);
        bundleSendSearchRequest.putString("bulan", bulan);
        bundleSendSearchRequest.putString("tahun", tahun);
        bundleSendSearchRequest.putString("cab", cab);
        bundleSendSearchRequest.putString("status", status);
        i.putExtras(bundleSendSearchRequest);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id ==  R.id.action_reconnect) {
            getCabangRequest();
        }
        else if (id ==  R.id.action_help) {
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.action_setting){
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            startActivity(intent);
        }
        else if(id ==  R.id.action_logout){
            session.logoutUser();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void getCabangRequest(){
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading, Please Wait...");
        progressDialog.show();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(APP_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());
        final Retrofit retrofit = builder.build();

        AppInterface.ListCabangInterface ReqList = retrofit.create(AppInterface.ListCabangInterface.class);
        Call<ResCabang> call = (Call<ResCabang>) ReqList.getCabangData();
        call.enqueue(new Callback<ResCabang>() {
            @Override
            public void onResponse(Call<ResCabang> call, Response<ResCabang> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    ResCabang mCabang = response.body();
                    List<ResCabangData> mCabangData = response.body().getCabangData();
                    AppAdapterSpinner spCabangAdapter = new AppAdapterSpinner(mCabangData);
                    spCabang.setAdapter(spCabangAdapter);
                    spCabang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            ResCabangData cabang = (ResCabangData) parent.getSelectedItem();
                            sCabang = cabang.getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                } else if (!response.isSuccessful()) {
                    Gson gson       = new GsonBuilder().create();
                    ResError mError = new ResError();
                    try {
                        mError=gson.fromJson(response.errorBody().string(),ResError.class);
                        Toast.makeText(MainActivity.this, mError.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResCabang> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(mMainFormView, "Ups, Something Wrong. Please check connection or any problem in your device", Snackbar.LENGTH_LONG).show();
            }
        });
    }
}