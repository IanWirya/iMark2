package com.halalinaja.imark2.View;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.halalinaja.imark2.AppConfig.AppInterface;
import com.halalinaja.imark2.AppConfig.AppSessionManager;
import com.halalinaja.imark2.AppConfig.AppAdapterRecycle;
import com.halalinaja.imark2.Model.ResDoctor;
import com.halalinaja.imark2.Model.ResDoctorData;
import com.halalinaja.imark2.Model.ResError;
import com.halalinaja.imark2.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.halalinaja.imark2.AppConfig.AppGlobalVariable.APP_BASE_URL;

public class ListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AppAdapterRecycle adapter;
    private ResDoctor mDoctor;
    private List<ResDoctorData> mDoctorData;
    private View mListActivityView;
    private AppSessionManager session;
    private String user,bulan,tahun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        session = new AppSessionManager(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarlist);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_main);
        getSupportActionBar().setTitle(getString(R.string.title_activity_list_dokter));

        mListActivityView   = findViewById(R.id.activity_list);

        Bundle bundleSendSearchRequest = getIntent().getExtras();
        String user = bundleSendSearchRequest.getString("user");
        String cab = bundleSendSearchRequest.getString("cab");
        String bulan = bundleSendSearchRequest.getString("bulan");
        String tahun = bundleSendSearchRequest.getString("tahun");
        String nama_dokter = bundleSendSearchRequest.getString("nama_dokter");
        String status = bundleSendSearchRequest.getString("status");

        sendSearchRequest(user, cab, bulan, tahun, nama_dokter, status);
        preparingRecycleView();
    }

    private void preparingRecycleView(){
        recyclerView = (RecyclerView) findViewById(R.id.card_recView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                //Values are passing to activity & to fragment as well
                Intent i = new Intent(ListActivity.this, DetailActivity.class);
                Bundle bundleSendSearchRequest = new Bundle();
                bundleSendSearchRequest.putString("kode_dokter", mDoctorData.get(position).getKodeDokter());
                bundleSendSearchRequest.putString("nama_dokter", mDoctorData.get(position).getNamaDokter());
                bundleSendSearchRequest.putString("bulan", bulan);
                bundleSendSearchRequest.putString("tahun", tahun);
                bundleSendSearchRequest.putString("user", user);
                bundleSendSearchRequest.putString("alamat1", mDoctorData.get(position).getAlamat1());
                bundleSendSearchRequest.putString("alamat2", mDoctorData.get(position).getAlamat2());
                bundleSendSearchRequest.putString("alamat3", mDoctorData.get(position).getAlamat3());
                i.putExtras(bundleSendSearchRequest);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(ListActivity.this, "Long press on position :"+(position+1), Toast.LENGTH_LONG).show();
            }
        }));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id ==  R.id.action_help) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.action_setting){
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
        else if(id ==  R.id.action_logout){
            session.logoutUser();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendSearchRequest(String user, String cab, String bulan, String tahun, String nama_dokter, String status) {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(ListActivity.this);
        progressDialog.setMessage("Loading, Please Wait...");
        progressDialog.show();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(APP_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());
        final Retrofit retrofit = builder.build();

        AppInterface.ListDMInterface ReqList = retrofit.create(AppInterface.ListDMInterface.class);
        Call<ResDoctor> call = (Call<ResDoctor>) ReqList.ListDoctorByMarket(user, cab, bulan, tahun, nama_dokter, status);
        call.enqueue(new Callback<ResDoctor>() {
            @Override
            public void onResponse(Call<ResDoctor> call, Response<ResDoctor> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    mDoctor = response.body();
                    mDoctorData = mDoctor.getDoctorData();
                    adapter = new AppAdapterRecycle(mDoctorData);
                    recyclerView.setAdapter(adapter);
                    System.out.println(mDoctorData);
                } else if (!response.isSuccessful()) {
                    Gson gson       = new GsonBuilder().create();
                    ResError mError = new ResError();
                    try {
                        mError=gson.fromJson(response.errorBody().string(),ResError.class);
                        Toast.makeText(ListActivity.this, mError.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ListActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResDoctor> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(mListActivityView, "Ups, Something Wrong. Please check connection or any problem in your device", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public static interface ClickListener{
        public void onClick(View view,int position);
        public void onLongClick(View view,int position);
    }

    /**
     * RecyclerView: Implementing single item click and long press (Part-II)
     *
     * - creating an innerclass implementing RevyvlerView.OnItemTouchListener
     * - Pass clickListener interface as parameter
     * */

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}