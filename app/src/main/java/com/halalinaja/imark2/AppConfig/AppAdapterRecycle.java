package com.halalinaja.imark2.AppConfig;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.halalinaja.imark2.Model.ResDoctorData;
import com.halalinaja.imark2.R;

import java.util.List;

public class AppAdapterRecycle extends RecyclerView.Adapter<AppAdapterRecycle.ViewHolder>{

    private List<ResDoctorData> doctor;
    public AppAdapterRecycle(List<ResDoctorData> doctor){
        this.doctor = doctor;
    }

    @Override
    public AppAdapterRecycle.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AppAdapterRecycle.ViewHolder viewHolder, int i) {
        viewHolder.nama_dokter.setText(doctor.get(i).getNamaDokter());
        viewHolder.alamat_1.setText(doctor.get(i).getAlamat1());
        viewHolder.alamat_2.setText(doctor.get(i).getAlamat2());
        viewHolder.alamat_3.setText(doctor.get(i).getAlamat3());
        viewHolder.total_ps.setText("Ps : "+doctor.get(i).getTotalPs());
        viewHolder.total_omzet.setText("Omzet : "+doctor.get(i).getTotalOmzet());
        viewHolder.total_pasien.setText("Pasien : "+doctor.get(i).getTotalPasien());
    }

    @Override
    public int getItemCount() {
        return doctor.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private  TextView nama_dokter, alamat_1, alamat_2, alamat_3, total_ps, total_omzet, total_pasien;
        public ViewHolder(View view) {
            super(view);
            nama_dokter = (TextView)view.findViewById(R.id.nama_dokter);
            alamat_1 = (TextView)view.findViewById(R.id.alamat_1);
            alamat_2 = (TextView)view.findViewById(R.id.alamat_2);
            alamat_3 = (TextView)view.findViewById(R.id.alamat_3);
            total_ps = (TextView)view.findViewById(R.id.total_ps);
            total_omzet = (TextView)view.findViewById(R.id.total_omzet);
            total_pasien = (TextView)view.findViewById(R.id.total_pasien);
        }
    }
}
