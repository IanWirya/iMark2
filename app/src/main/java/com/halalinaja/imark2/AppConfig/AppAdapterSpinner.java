package com.halalinaja.imark2.AppConfig;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.halalinaja.imark2.Model.ResCabangData;

import java.util.List;

public class AppAdapterSpinner extends BaseAdapter implements SpinnerAdapter {
    private final List<ResCabangData> data;

    public AppAdapterSpinner(List<ResCabangData> data){
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    @Override
    public View getView(int position, View recycle, ViewGroup parent) {
        TextView text;
        if (recycle != null){
            // Re-use the recycled view here!
            text = (TextView) recycle;
        } else {
            // No recycled view, inflate the "original" from the platform:;
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            text = (TextView) inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }
        text.setTextColor(Color.BLACK);
        text.setText(data.get(position).getNama());
        return text;
    }
}