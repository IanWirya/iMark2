package com.halalinaja.imark2.AppConfig;

import com.halalinaja.imark2.Model.ResCabang;
import com.halalinaja.imark2.Model.ResDoctor;
import com.halalinaja.imark2.Model.ResSuccess;
import com.halalinaja.imark2.Model.ResUser;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AppInterface {

    public interface LoginInterface {
        @FormUrlEncoded
        @POST("login")
        Call<ResUser> LoginUser (
                @Field("user") String user,
                @Field("pass") String pass
        );
    }

    public interface ListCabangInterface {
        @GET("cabang")
        Call<ResCabang> getCabangData();

    }

    public interface ListDMInterface {
        @FormUrlEncoded
        @POST("listdoctorbymarket")
        Call<ResDoctor> ListDoctorByMarket (
                @Field("user") String user,
                @Field("cab") String cab,
                @Field("bulan") String bulan,
                @Field("tahun") String tahun,
                @Field("nama_dokter") String nama_dokter,
                @Field("status") String status
        );
    }

    public interface ListDDInterface {
        @FormUrlEncoded
        @POST("listdoctorbydoctor")
        Call<ResDoctor> ListDoctorByDoctor (
                @Field("kode_dokter") String kode_dokter,
                @Field("cab") String cab,
                @Field("bulan") String bulan,
                @Field("tahun") String tahun,
                @Field("status") String status
        );
    }

    public interface UpdateKunjunganInterface {
        @FormUrlEncoded
        @POST("visit")
        Call<ResSuccess> UpdateKunjungan (
                @Field("kode_dokter") String kode_dokter,
                @Field("bulan") String bulan,
                @Field("tahun") String tahun,
                @Field("user") String user,
                @Field("masukkan") String masukkan,
                @Field("latitude") String latitude,
                @Field("longitude") String longitude,
                @Field("ket_det") String ket_det
        );
    }
}
